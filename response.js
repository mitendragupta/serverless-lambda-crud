// function response(statusCode, message) {
//   return {
//     statusCode: statusCode,
//     body: JSON.stringify({ results: message })
//   }
// }
module.exports = (statusCode,msgtype, message) => {
    return {
        statusCode: statusCode,
        body: JSON.stringify({ [msgtype]: message })
    }
};