'use strict';
//const mysql = require('mysql2');
const connectionstr = require('./dbconfig/connection');//require('mysql2');
const uuidv4 = require('uuid/v4');
const response = require('./response'); // response generater function.
//const encrypttext = require('./emailencrypt'); // email encrypt module
//const decrypttext = require('./emaildecrypt'); // email decrypt module
const encodeDecode = require('./encryptdecrypt');
const { Validator } = require('node-input-validator');
const bcrypt = require('bcryptjs');
//const bcrypt = require('bcrypt');
const saltRounds = 10;


//getsingleuser
module.exports.getuser = async (event, context, callback) => {
  //get the client
  const mysql = require('mysql2/promise');
  const uid = event.pathParameters.uid;
  //create the connection
  try {
    const connection = await mysql.createConnection(connectionstr);
    // query database
    let [results, buffer] = await connection.execute('SELECT * FROM users where id = ?', [uid]);
    //return results  
    connection.end();
    if (results.length > 0) {
      let data = {};
      results.forEach(items => {
        data['id'] = items.id,
          data['firstname'] = items.firstname,
          data['lastname'] = items.lastname,
          data['mobilenumber'] = items.mobilenumber,
          data['encryptedemail'] = items.email,
          data['originalemail'] = encodeDecode(items.email)// decrypttext(items.email)
      });
      callback(null, response(200, 'success', data));

    } else {
      callback(null, response(200, 'success', 'No data found.'));
    }

  } catch (err) {
    callback(null, response(err.code, 'error', err.message));
  }
};
//getalluser
module.exports.getallusers = async (event, context, callback) => {
  //get the client
  //console.log('test');
  const mysql = require('mysql2/promise');
  //create the connection
  try {
    const connection = await mysql.createConnection(connectionstr);
    let [results, buffer] = await connection.execute('SELECT * FROM users');
    //return results  
    connection.end();
    if (results.length > 0) {
      let userslist = [];
      results.forEach(items => {
        let data = {};
        data['id'] = items.id,
          data['firstname'] = items.firstname,
          data['lastname'] = items.lastname,
          data['mobilenumber'] = items.mobilenumber,
          data['encryptedemail'] = items.email,
          data['originalemails'] = encodeDecode(items.email)//decrypttext(items.email)
        userslist.push(data);
      });
      callback(null, response(200, 'success', userslist));
    } else {
      callback(null, response(200, 'success', 'No data found.'));
    }

  } catch (err) {
    callback(null, response(err.code, 'error', err.message));
  }
};
//create users
module.exports.createuser = async (event, context, callback) => {
  // get the client
  const formData = JSON.parse(event.body);
  const mysql = require('mysql2/promise');

  //let error = [];
  // if (formData.firstname == '') {
  //   error.push('Please enter first name.');
  // }
  // if (formData.lastname == '') {
  //   error.push('Please enter last name.');
  // }
  // if (formData.mobilenumber == '') {
  //   error.push('Please enter moblie number.');
  // }
  // if (formData.mobilenumber != '') {
  //   let mobilenumber = formData.mobilenumber;
  //   let cellnumber = /^\d{10}$/;
  //   if (!cellnumber.test(mobilenumber)) {
  //     error.push("Invalid number; must be ten digits");
  //   }
  // }
  // if (formData.email == '') {
  //   error.push('Please enter email.');
  // }
  // if (formData.email != '') {
  //   var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  //   if (!formData.email.match(mailformat)) {
  //     error.push('You have entered an invalid email address.');
  //   }
  // }
  const v = new Validator(formData, {
    firstname: 'required',
    lastname: 'required',
    mobilenumber: 'required|maxLength:10|integer',
    email: 'required|email',
    password: 'required|maxLength:15'
  });

  const matched = await v.check();
  if (!matched) {
    // console.log(v);
    callback(null, response(422, 'error', v.errors));
  } else {
    try {
      // create the connection
      const connection = await mysql.createConnection(connectionstr);
      // query database
      let id = uuidv4();
      //formData.email = encrypttext(formData.email);
      formData.email = encodeDecode(formData.email);
      //check email address if already exist.
      let [users, buff] = await connection.execute('SELECT count(id) as uid FROM users where email = ?', [formData.email]);
      //console.log();
      let userid = users[0].uid;
      console.log("testing" + userid);
      if (userid) {
        connection.end();
        return callback(null, response(409, 'error', 'Email is already registerd!'));
      } else {
        var password = bcrypt.hashSync(formData.password, saltRounds);
        let [results, buffer] = await connection.execute('INSERT INTO users (id,firstname, lastname, mobilenumber,email,password) VALUES (?,?,?,?,?,?)',
          [id, (formData.firstname !== undefined) ? formData.firstname : null, (formData.lastname !== undefined) ? formData.lastname : null, (formData.mobilenumber !== undefined) ? formData.mobilenumber : null, (formData.email !== undefined) ? formData.email : null, password]);
        // callback(null, response(200, results));
        connection.end();
        // console.log(results);
        if (results.affectedRows > 0) {
          callback(null, response(200, 'success', 'Record inserted successfully.'));
        } else {
          callback(null, response(500, 'error', 'Something error in database.'));
        }
      }
    } catch (err) {
      callback(null, response(err.code, 'error', err.message));
    }
  }
};
module.exports.updateuser = async (event, context, callback) => {
  // get the client
  const formData = JSON.parse(event.body);
  const uid = formData.id;
  const mysql = require('mysql2/promise');
  // let error = [];
  // if (formData.firstname == '') {
  //   error.push('Please enter first name.');
  // }
  // if (formData.lastname == '') {
  //   error.push('Please enter last name.');
  // }
  // if (formData.mobilenumber == '') {
  //   error.push('Please enter moblie number.');
  // }
  // if (formData.mobilenumber != '') {
  //   let mobilenumber = formData.mobilenumber;
  //   let cellnumber = /^\d{10}$/;
  //   if (!cellnumber.test(mobilenumber)) {
  //     error.push("Invalid number; must be ten digits.");
  //   }
  // }
  // if (formData.email == '') {
  //   error.push('Please enter email.');
  // }
  // if (formData.email != '') {
  //   var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  //   if (!formData.email.match(mailformat)) {
  //     error.push('You have entered an invalid email address.');
  //   }
  // }
  const v = new Validator(formData, {
    id: 'required',
    firstname: 'required',
    lastname: 'required',
    mobilenumber: 'required|maxLength:10|integer',
    email: 'required|email'
  });

  const matched = await v.check();
  // if (error != '') {
  //   callback(null, response(400, 'error', error));
  // } 
  if (!matched) {
    callback(null, response(422, 'error', v.errors));
  } else {
    try {
      // create the connection
      const connection = await mysql.createConnection(connectionstr);
      // query database
      //formData.email = encrypttext(formData.email);
      formData.email = encodeDecode(formData.email);
      // let [results, buffer] = await connection.execute('update users set firstname =?,lastname =?,mobilenumber =?,email=? where id=?',
      //   [formData.firstname, formData.lastname, formData.mobilenumber, encryptedemail, uid]);
      //check email address if already exist.
      let [users, buff] = await connection.execute('SELECT count(id) as uid FROM users where email = ? and id <> ?', [formData.email, uid]);
      //console.log(users);
      let userid = users[0].uid;
      if (userid) {
        return callback(null, response(409, 'error', 'Email is already registerd!'));
      } else {
        let [results, buffer] = await connection.execute('update users set firstname =?,lastname =?,mobilenumber =?,email=? where id=?',
          [(formData.firstname !== undefined) ? formData.firstname : null, (formData.lastname !== undefined) ? formData.lastname : null, (formData.mobilenumber !== undefined) ? formData.mobilenumber : null, (formData.email !== undefined) ? formData.email : null, uid]);
        //callback(null, response(200, results));
        connection.end();
        if (results.changedRows > 0) {
          callback(null, response(200, 'success', 'Record updated successfully.'));
        } else {
          callback(null, response(200, 'success', 'Same data you have updated.'));
        }
      }
    } catch (err) {
      callback(null, response(err.code, 'error', err.message));
    }
  }
};
//delete user function
module.exports.deleteuser = async (event, context, callback) => {
  const mysql = require('mysql2/promise');
  const uid = event.pathParameters.uid;
  //create the connection
  try {
    const connection = await mysql.createConnection(connectionstr);
    // query database
    let [results, buffer] = await connection.execute('DELETE FROM users where id = ?', [uid]);
    connection.end();
    //return results  
    if (results.affectedRows > 0) {
      callback(null, response(200, 'success', 'Record deleted successfully.'));
    } else {
      callback(null, response(400, 'error', 'Invalid request'));
    }

  } catch (err) {
    callback(null, response(err.code, 'error', err.message));
  }
};
