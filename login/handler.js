'use strict';
const mysql = require('mysql2');
const connectionstr = require('../dbconfig/connection');//require('mysql2');
const response = require('../response'); // response generater function.
const encodeDecode = require('../encryptdecrypt');
const { Validator } = require('node-input-validator');
const bcrypt = require('bcryptjs');
//const bcrypt = require('bcrypt');
//user login
module.exports.loginuser = async (event, context, callback) => {
  //get the client
  const mysql = require('mysql2/promise');
  const formData = JSON.parse(event.body);
  //console.log(formData);
  const v = new Validator(formData, {
    email: 'required|email',
    password: 'required'
  });
  const matched = await v.check();
  if (!matched) {
    // console.log(v);
    callback(null, response(422, 'error', v.errors));
  } else {
    //create the connection
    try {
      const connection = await mysql.createConnection(connectionstr);
      // query database
      let [results, buffer] = await connection.execute('SELECT * FROM users where email = ?', [encodeDecode(formData.email)]);
      //return results  
      connection.end();
      if (results.length > 0) {
        let match = bcrypt.compareSync(formData.password, results[0].password); // true
        if (match) {
          let data = {};
          results.forEach(items => {
            data['id'] = items.id,
              data['firstname'] = items.firstname,
              data['lastname'] = items.lastname,
              data['mobilenumber'] = items.mobilenumber,
              data['encryptedemail'] = items.email,
              data['originalemail'] = encodeDecode(items.email)// decrypttext(items.email)
          });
          callback(null, response(200, 'success', data));
        } else {
          callback(null, response(401, 'error', 'You have entered incorrect password.'));
        }
      } else {
        callback(null, response(401, 'error', "Email address doesn't exit."));
      }
    } catch (err) {
      callback(null, response(err.code, 'error', err.message));
    }
  }
};